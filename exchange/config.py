API_KEY = '842c062c18b62200d3cc94e3271d71a3'

rules = {
    'archive': True,
    'email': {
        'enable': True,
        'preferred': ['BTC', 'IRR', 'USD', 'AFN'],
        'receiver': 'A Test User <to@example.com>',
    },
    'sms': {
        'enable': True,
        'preferred': {
            'USD': {'min': 1.10, 'max': 1.51},
            'IRR': {'min': 49234.739038, 'max': 52234.739038},
        },
        'receiver': '09309806535'
    },
}

API = ('61635667667A384A4849684E5655526336413631427639426E4F6'
       '7715765757473412F324757376E4A42453D')

