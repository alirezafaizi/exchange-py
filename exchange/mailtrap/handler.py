import smtplib

from email.mime.text import MIMEText

from config import rules

import json

from khayyam import JalaliDatetime


def smtp_send_mail(subject, body):
    msg = MIMEText(body)
    msg['subject'] = subject
    msg['from'] = 'Private Person <from@example.com>'
    msg['to'] = rules['email']['receiver']

    with smtplib.SMTP('smtp.mailtrap.io', 2525) as server:
        server.login('d51a0b24a0b536', '5313b4d0c03cb2')
        server.sendmail(msg['from'], msg['to'], msg.as_string())


def send_mail(filename, rates):
    """
    send email
    :param filename: timestamp
    :param rates: rates
    :return: None
    """
    now = JalaliDatetime.now()
    date = now.strftime('%y-%m-%d %A %H:%M')
    subject = f'timestamp : {filename} - {date}'
    response = rules['email']['preferred']
    if response:
        tmp = dict()
        for exc in response:
            tmp[exc] = rates[exc]
        rates = tmp
    text = json.dumps(rates)

    smtp_send_mail(subject, text)
