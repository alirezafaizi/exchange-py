import json

from config import API_KEY, rules, API

from khayyam import JalaliDatetime

from fixer import get_rates

from mailtrap import send_mail

from sms_kavenegar import send_sms


def archive(filename, rates):
    """
    backup rates
    :param filename: timestamp
    :param rates: rates
    :return: None
    """
    with open(f'archive/{filename}.json', 'w') as f:
        f.write(json.dumps(rates))


def get_text_sms(rates):
    """
    get body sms
    :param rates:
    :return: msg
    """
    preferred = rules['sms']['preferred']
    msg = str()
    for exc in preferred:
        if rates[exc] <= preferred[exc]['min']:
            msg += f'{exc} reached min {rates[exc]}'
        if rates[exc] >= preferred[exc]['max']:
            msg += f'{exc} reached max {rates[exc]}'

        return msg


def send_sms_k(text):
    now = JalaliDatetime.now()
    date = now.strftime('%y-%m-%d  %A  %H:%M')
    text += date
    send_sms(text, API)


if __name__ == '__main__':
    res = get_rates(API_KEY)

    if rules['archive']:
        archive(res['timestamp'], res['rates'])

    if rules['email']['enable']:
        send_mail(res['timestamp'], res['rates'])

    if rules['sms']['enable']:
        notification_msg = get_text_sms(res['rates'])
        if notification_msg:
            send_sms_k(notification_msg)
