import requests
import json

API = 'http://data.fixer.io/api/latest?access_key='


def get_rates(API_KEY):
    """
    get rates from site
    :return: json.loads(response.text) and None
    """
    response = requests.get(API + API_KEY)
    if response.status_code == 200:
        return json.loads(response.text)
    return None

